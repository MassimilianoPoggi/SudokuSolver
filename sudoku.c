#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct sudoku_cell {
	int number;
	bool possible[9];
};

struct sudoku {
	struct sudoku_cell cells[81];
};

void init_sudoku(struct sudoku *s) {
	int i, number;
	for (i = 0; i < 81; i++) {
		s->cells[i].number = 0;

		for (number = 0; number < 9; number++) 
			s->cells[i].possible[number] = true;
	}
}

void print_sudoku(struct sudoku* s) {
	int i, j;
	for (i = 0; i < 81; i++) {
		if (i % 27 == 0) 
			printf("-------------------------\n");

		if (i % 3 == 0) 
			printf("| ");

		printf("%d ", s->cells[i].number);

		if (i % 9 == 8)
			printf("|\n");
	}

	printf("-------------------------\n");
}

void add_number(struct sudoku *s, int index, int number) {
	int i;	
	int row = index / 9;
	int column = index % 9;
	s->cells[index].number = number+1;
	for (i = 0; i < 9; i++) {
		s->cells[i*9 + column].possible[number] = false;
		s->cells[row*9 + i].possible[number] = false;
		s->cells[((row - row % 3) + (i/3))*9 + (column - column % 3 + i % 3)].possible[number] = false;
	}
}

int check_sector(struct sudoku *s, int index, int (*progression)(int, int)) {
	int i, number;
	int count = 0;

	for (number = 0; number < 9; number++) {
		int possibles = 0;
		int cell = 0;

		for (i = 0; i < 9; i++) {
			int cell_index = progression(index, i);

			if (s->cells[cell_index].number == 0 && s->cells[cell_index].possible[number]) {
				possibles++;
				cell = cell_index;
			}
		}

		if (possibles == 1) {
			add_number(s, cell, number);
			count++;
		}
	}	

	return count;
}

int column_progression(int index, int i) {
	return i * 9 + index;
}

int row_progression(int index, int i) {
	return index * 9 + i;
}

int quadrant_progression(int index, int i) {
	int row = index  - index % 3;
	int column = (index % 3) * 3;

	return (row + (i / 3)) * 9 + column + i % 3;
}

int check_single_cells(struct sudoku *s) {
	int i, j;
	int count = 0;

	for (i = 0; i < 81; i++) {
		if (s->cells[i].number != 0)
			continue;

		int possibles = 0;
		int number = 0;

		for (j = 0; j < 9; j++) {
			if (s->cells[i].possible[j]) {
				number = j;
				possibles++;
			}
		}

		if (possibles == 1) {
			add_number(s, i, number);
			count++;
		}
	}

	return count;
}

int solve_smart(struct sudoku *s) {
	int i, count, result;
	do {
		count = 0;
		for (i = 0; i < 9; i++) {
			count += check_sector(s, i, row_progression);
			count += check_sector(s, i, column_progression);
			count += check_sector(s, i, quadrant_progression);
		}

		count += check_single_cells(s);
		result += count;
		
	} while(count > 0);

	return result;
}

bool is_possible_bruteforce(struct sudoku *s, int index, int number) {
	int i;
	int column = index % 9;
	int row = index / 9;
	int quadrant = row  - row % 3 + column / 3;
	for (i = 0; i < 9; i++) {
		if (s->cells[row_progression(row, i)].number == number + 1) {
			return false;
		}
		if (s->cells[column_progression(column, i)].number == number + 1) {
			return false;
		}
		if (s->cells[quadrant_progression(quadrant, i)].number == number + 1) {
			return false;
		}
	}
	return true;
}

bool solve_bruteforce(struct sudoku *s, int index) {
	if (index >= 81)
		return true;

	if (s->cells[index].number != 0)
		return solve_bruteforce(s, index + 1);

	int i;
	for (i = 0; i < 9; i++) {
		if (is_possible_bruteforce(s, index, i)) {
			s->cells[index].number = i+1;

			if (solve_bruteforce(s, index+1))
				return true;

			s->cells[index].number = 0;
		}
	}

	return false;
}

int main(int argc, char** argv) {
	struct sudoku s;
	init_sudoku(&s);

	add_number(&s, 0, 7);
	add_number(&s, 1, 2);
	add_number(&s, 7, 3);
	add_number(&s, 8, 5);
	add_number(&s, 10, 1);
	add_number(&s, 12, 0);
	add_number(&s, 14, 3);
	add_number(&s, 16, 2);
	add_number(&s, 29, 1);
	add_number(&s, 30, 8);
	add_number(&s, 32, 5);
	add_number(&s, 33, 4);
	add_number(&s, 36, 0);
	add_number(&s, 37, 3);
	add_number(&s, 43, 1);
	add_number(&s, 44, 2);
	add_number(&s, 47, 4);
	add_number(&s, 48, 3);
	add_number(&s, 50, 2);
	add_number(&s, 51, 0);
	add_number(&s, 64, 5);
	add_number(&s, 66, 2);
	add_number(&s, 68, 7);
	add_number(&s, 70, 6);
	add_number(&s, 72, 8);
	add_number(&s, 73, 4);
	add_number(&s, 79, 5);
	add_number(&s, 80, 1);

	int cells_filled = 28;
	cells_filled += solve_smart(&s);

	if (cells_filled < 81) {
		printf("Smart solver got to:\n");
		print_sudoku(&s);
		printf("Starting bruteforce solver.\n");

		if (solve_bruteforce(&s, 0)) {
			printf("Bruteforce solver found the solution:\n");
			print_sudoku(&s);
		} else
			printf("Bruteforce solver failed to resolve the puzzle.\n");
	}
	else {
		printf("Solution found:\n");
		print_sudoku(&s);
	}
}